# MRCNN_TF_ROS

Mask RCNN implementation for PAlbator, working with ROS

In a catkin workspace, clone this git repo within a src folder

cd src/<git repo>

git checkout <branch>

At the root of the catkin workspace, do :

catkin_make

Don't forget to source.

#Warning

Camera topic subscribed : /camera/rgb/image_raw

The config file for the launch file is not properly working for now!

#Launch 

roslaunch own_RCNN projet.launch

#Interesting Topics

#output image

/detection_mask_RCNN/publish_image_output

#mask info (label, score, mask x and y coordinates)

/detection_mask_RCNN/publish_message_output_mask
